<?php namespace ProcessWire;
/*
  This file is part of TagHerder.

  TagHerder is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  TagHerder is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with TagHerder. If not, see <https://www.gnu.org/licenses/>.
*/

class ProcessTagHerder extends Process {
  public static function getModuleInfo() {
    return [
      'title'   => 'Process Tag Herder',
      'summary' => 'Helps organise tags',
      'version' => 1,
      'author' => 'Rich Lott / Artful Robot',
      'icon' => 'tag',
      // 'permission' => 'helloworld',
      // page that you want created to execute this module
      'page' => [
        'name' => 'tag-herder',
        'parent' => 'setup',
        'title' => 'Tag Herder'
      ],
    ];
  }

  public function execute() {

    $returnType = 'normal';
    $message = '';
    if (input()->requestMethod('POST')) {
      $returnType = 'htmlOnly';

      $input = json_decode(file_get_contents('php://input'), TRUE);
      $tagToDelete = $input['tagToDelete'] ?? NULL;
      $replaceTagID = $input['replaceTagID'] ?? NULL;
      $tagToRename = $input['tagToRename'] ?? NULL;
      if (!empty($tagToRename)) {
        $newName = (string) ($input['newName'] ?? '');
        if (!$newName) {
          http_response_code(400);
          echo "Missing name";
          exit;
        }
        $tag = $this->loadTag($tagToRename);
        $tag->of(false);
        $tag->title = $newName;
        pages()->saveField($tag, 'title');
        // $message = "Renamed";
      }
      if (!empty($replaceTagID)) {
        $replaceTag = $this->loadTag($replaceTagID);
        $survivingTag = $this->loadTag($input['survivingTagID']);
        $i = 0;
        foreach (pages()->find("tags=$replaceTag") as $page) {
          /** @var Page $page */
          $page->of(false);
          /** @var PageArray $tags */
          $tags = $page->getUnformatted('tags')->remove('id=$replaceTagID')->append($survivingTag);
          $page->setUnformatted('tags', $tags);
          pages()->saveField($page, 'tags');
          $i++;
        }
        pages()->delete($replaceTag);
        // $message = "$i pages updated";
      }
      else if ($tagToDelete) {
        $tag = $this->loadTag($tagToDelete);
        pages()->trash($tag);
      }
    }

    // List all tags, and count their children.
    $tags = pages('template=tag, sort=title');
    $list = [];
    foreach ($tags as $tag) {
      $pagesWithThisTag = pages("tags=$tag")->each('<li><a href="{url}">{title}</a></li>');
      if (empty($tag->getUnformatted('title'))) {
        $tag->setUnformatted('title', '(missing title!)');
      }
      $list[] = $tag->getFormatted(<<<HTML
        <li data-tag-id="{id}" data-tag-title="{title}" ><strong>{title}</strong> <a href={editUrl}>Edit</a>
        <button class=rename>Rename</button> <button class=trash>Trash</button> <button class=replace>Replace</button>
        <ol>$pagesWithThisTag</ol></li>
        HTML);
    }
    $list = "$message<ul id=tagherder-list>" . implode("\n", $list) . "<ul>";
    $list .= "<span id=tagherder-replacer><label>Replace with<select><option value=''>--Select replacement--</option>"
      . $tags->each('<option value={id} >{title}</option>')
      . "</select></label></span>";


    if ($returnType === 'normal') {
      $list .= '<script>' . file_get_contents(__DIR__ . '/js/tagHerder.js') . '</script>';
    }
    else {
      echo $list;
      exit;
    }

    return $list;
  }

  public function loadTag($id): Page {
    $tag = pages()->get('template=tag, id=' . $id);
    if (!$tag) {
      http_response_code(404);
      echo "Tag not found";
      exit;
    }
    if (!$tag->deletable()) {
      http_response_code(401);
      echo "You don't have permission to do that.";
      exit;
    }
    return $tag;
  }
}

