/*
  This file is part of TagHerder.

  TagHerder is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  TagHerder is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with TagHerder. If not, see <https://www.gnu.org/licenses/>.

  Source: https://codeberg.org/artfulrobot/TagHerder
*/
document.addEventListener('DOMContentLoaded', bootTagHerder);
function bootTagHerder() {
  const ui = document.getElementById('tagherder-replacer');
  const replacerSelect = ui.querySelector('select');
  const listUl = document.getElementById('tagherder-list');

  const uiState = (d) => {
    [].forEach.call(listUl.querySelectorAll('button'), b => b.disabled = d);
    console.log({ui});
    ui.style.display = 'none';
  };
  const replaceBodyWithResponse = response => {
    if (response.status !== 200) {
      throw {response};
    }
    return response.text().then(r => {
        console.log('replaceBodyWithResponse', r);
        listUl.outerHTML = r;
        bootTagHerder();
        console.log("done");
    });
  };
  const errorResponse = e => {
    console.warn('errorResponse', e);
    if (e.response && e.response.status < 500) {
      return e.response.text().then(r => alert);
    }
    else {
      alert("Network error.");
    }
    uiState(false);
  };

  ui.style.display = 'none';
  replacerSelect.addEventListener('change', e => {
    const newTag = ui.querySelector(`option:checked`).textContent;
    uiState(true);
    if (confirm(`Tag all pages currently tagged '${ui.selectedTag.clickedTagTitle}' with '${newTag}' instead?`)) {
      fetch('/pnpadmin/setup/tag-herder/', {method: 'POST',
        // Content-Type with a value of application/x-www-form-urlencoded, multipart/form-data, or text/plain
        body: JSON.stringify({ replaceTagID: ui.selectedTag.clickedTagId, survivingTagID: replacerSelect.value })})
      // .then(response => response.json()) // <<<---note this
      .then(replaceBodyWithResponse).catch(errorResponse);
    }
    else {
      uiState(false);
    }
  });
  listUl.addEventListener('click', e => {
    const clickedTagLi = e.target.closest('li');
    const clickedTagId = clickedTagLi.dataset.tagId;
    const clickedTagTitle = clickedTagLi.dataset.tagTitle;
    ui.selectedTag = {clickedTagId, clickedTagTitle, clickedTagLi};
    if (e.target.classList.contains('rename')) {
      const newName = prompt(`Rename '${clickedTagTitle}' to?`, clickedTagTitle);
      uiState(true);
      if (!newName || newName === clickedTagTitle) {
        uiState(false);
        return;
      }
      fetch('/pnpadmin/setup/tag-herder/',
        {
          method: 'POST',
          body: JSON.stringify({ tagToRename: clickedTagId, newName }),
          headers: { 'Content-Type': 'application/json' }
        })
        .then(replaceBodyWithResponse).catch(errorResponse);
    }
    if (e.target.classList.contains('trash')) {
      uiState(true);
      if (confirm(`Delete "${clickedTagTitle}" (${clickedTagId})?`)) {
        // ok
        console.log("Deleting ", clickedTagId, JSON.stringify({ tagToDelete: clickedTagId}));
        fetch('/pnpadmin/setup/tag-herder/',
        {
          method: 'POST',
            body: JSON.stringify({ tagToDelete: clickedTagId}),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(replaceBodyWithResponse).catch(errorResponse);
        }
        else {
          uiState(false);
        }
    }
    else if (e.target.classList.contains('replace')) {
      e.target.insertAdjacentElement('afterend', ui);
      ui.style.display = '';
    }
  });
}

