# Tag Herder ProcessWire module

This provides a page at **Setup » Tag Herder** that lists every tag
and every page tagged with that tag, and provides three options:

1. Rename the tag
2. Trash the tag
3. Replace the tag with another

The latter will add the selected replacement to all the pages, then *delete* the orginal tag.

It assumes/requires that the field that contains a page's tags is called `tags` and that the tags themselves are of template type `tag`.

It's quite crude, and if you have an enormous amount of tags/pages it's probably not a suitable tool!

Built for cleaning up a few hundred pages and hundred or so tags after a migration from another CMS.


## Copyright / License

Copyright Rich Lott / Artful Robot 2023

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

https://codeberg.org/artfulrobot/TagHerder
